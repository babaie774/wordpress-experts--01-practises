<?php

class Sample {
	protected static $instance_count;

	public function __construct() {
		self::$instance_count ++;
	}

	public static function get_counts() {
		return self::$instance_count;
	}

}

$random_numbers = mt_rand( 10, 100 );
while ( $random_numbers > 0 ) {
	new Sample();
	$random_numbers --;
}
echo 'counts : ' . Sample::get_counts();