<?php
require_once "db.class.php";

class User extends DB {

	//user roles
	const ADMIN = 1;
	const USER = 2;

	public function __construct() {
		parent::__construct();
		$this->table      = 'users';
		$this->primaryKey = 'id';
	}

	public function get_all_admin_users() {
		$this->stmt    = $this->connection->prepare( "SELECT * FROM {$this->table} WHERE role=:role" );
		$active_status = self::ADMIN;
		$this->stmt->bindParam( ':role', $active_status );

		return $this->stmt->execute();
	}

	public static function get_user_roles() {
		return array(
			self::ADMIN => 'مدیر',
			self::USER  => 'کاربر'
		);
	}

	public static function get_user_rol_formatted( $role ) {
		$roles = self::get_user_roles();

		return '<span>' . $roles[ $role ] . '</span>';
	}

}