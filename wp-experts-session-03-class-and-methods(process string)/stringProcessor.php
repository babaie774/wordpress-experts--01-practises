<?php

class stringProcessor {

	private $data;

	public function __construct( $data ) {
		$this->data = $data;
	}

	private function convertStringToArray() {
		$this->data = explode( ' ', $this->data );
	}

	private function convertToUppercase() {
		$this->data = array_map( function ( $item ) {
			return ucfirst( $item );
		}, $this->data );
	}

	private function printData() {
		foreach ( $this->data as $word ) {
			echo "<p>{$word}</p>";
		}
	}

	/**
	 * This approach called facade design pattern
	 */
	public function doOperation() {
		$this->convertStringToArray();
		$this->convertToUppercase();
		$this->printData();
	}

}

$a = new stringProcessor( 'sample text for test our class operation' );
$a->doOperation();