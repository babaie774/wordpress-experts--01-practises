<?php

class Autoloader {

	public function __construct() {
		spl_autoload_register( array( $this, 'autoload' ) );
	}

	public function autoload( $class_name ) {
		$file = $this->convert_class_to_file( $class_name );
		var_dump( $file );
		if ( is_file( $file ) && file_exists( $file ) && is_readable( $file ) ) {
			include $file;
		}
	}

	public function convert_class_to_file( $class_name ) {
		$class_name_parts = str_replace( "\\", DIRECTORY_SEPARATOR, $class_name );
		$class_name_parts = explode( DIRECTORY_SEPARATOR, $class_name_parts );
		$class            = end( $class_name_parts );
		unset( $class_name_parts[ count( $class_name_parts ) - 1 ] );
		$path = strtolower( implode( DIRECTORY_SEPARATOR, $class_name_parts ) );

		return __DIR__ . DIRECTORY_SEPARATOR . $path . DIRECTORY_SEPARATOR . $class . '.php';
	}

}

new Autoloader();
